-- Reveal.js backend for preslang
require "core"

local lreveal = { }
local properties = require("core")
-- some definitions used internally
lreveal.backend            = { }
lreveal.locale_prefix      = properties.preslang_path
lreveal.template_file      = lreveal.locale_prefix .. "assets/reveal_skeleton.html"
lreveal.backend.reveal_url = lreveal.locale_prefix .. "assets/reveal.js"
lreveal.reveal_slot        = "REVEALLOC"
lreveal.prop_slot          = "PRESLTITLE"
lreveal.body_slot          = "PRESLBODY"
lreveal.style_slot         = "PRESLTHEME"
-- base themes: beige, black, blood, league, moon, night, serif, simple, sky, solarized, white
lreveal.default_style = "simple"

-- syntactic replacement
lreveal.spacer = "<br/>"

-- helper functions

function lreveal.escape_text(string)
  return string -- TODO
end

function lreveal.wrap_flat_tag(tag,content)
  return "<" .. tag .. ">" .. lreveal.escape_text(properties.flatten_string(content)) .. "</" .. tag .. ">"
end

-- syntactic functions

function lreveal.slides(content)
  local final_content = ""
  local title = ""
  local style = lreveal.default_style
  -- extract properties
  if not (content[properties.title] == nil) then
    title = content[properties.title]
  end
  if not (content[properties.style] == nil) then
    style = content[properties.style]
  end
  -- read in template
  local template_file = assert(io.open(lreveal.template_file, "r"))
  local template = template_file:read("*all")
  template_file:close()
  -- accumulate body
  local body = ""
  for k,v in ipairs(content) do body = body .. v end
  -- perform template replacement
  final_content = template
  final_content = string.gsub(final_content,lreveal.reveal_slot,lreveal.backend.reveal_url)
  final_content = string.gsub(final_content,lreveal.prop_slot,title)
  final_content = string.gsub(final_content,lreveal.style_slot,style)
  final_content = string.gsub(final_content,lreveal.body_slot,body)
  return final_content
end

function lreveal.slide(content)
  local final_content = ""
  -- fish out the properties
  local bgcol  = ""
  if not (content[properties.bg] == nil) then
    bgcol = content[properties.bg]
    bgcol = " data-background-color=\"" .. bgcol .. "\""
  end
  local bgimg = ""
  if not (content[properties.bgimage] == nil) then
    bgimg = content[properties.bgimage]
    bgimg = " data-background-image=\"" .. bgimg .. "\""
  end
  local bgsize = ""
  if not (content[properties.bgsize] == nil) then
    bgsize = content[properties.bgsize]
    bgsize = " data-background-size=\"" .. bgsize .. "\""
  end
  local title = ""
  if not (content[properties.title] == nil) then
    title = content[properties.title]
    title = lreveal.wrap_flat_tag("h2",title)
  end
  -- construct the slide header
  final_content = "<section" .. bgcol .. bgimg .. bgsize .. ">" .. title
  -- construct the slide body
  for k,v in ipairs(content) do final_content = final_content .. v end
  -- close it
  final_content = final_content .. "</section>"
  return final_content
end

function lreveal.big_text(content)
  return lreveal.wrap_flat_tag("h3",content)
end

function lreveal.small_text(content)
  return "<font size=\"-1\">" .. properties.flatten_string(content) .. "</font>"
end

function lreveal.text(content)
  return lreveal.escape_text(properties.flatten_string(content))
end

function lreveal.list_items(content)
  res = ""
  for k,v in ipairs(content) do res = res .. lreveal.wrap_flat_tag("li",v) end
  return res
end

function lreveal.bullet_list(content)
  return lreveal.wrap_flat_tag("ul",lreveal.list_items(content))
end

function lreveal.number_list(content)
  return lreveal.wrap_flat_tag("ol",lreveal.list_items(content))
end

function lreveal.image(url)
  res = "<img"
  if not (url[properties.width] == nil) then
    res = res .. " width=\"" .. url[properties.width]  .. "\""
  end
  if not (url[properties.height] == nil) then
    res = res .. " height=\"" .. url[properties.height]  .. "\""
  end
  res = res .. " src=\"" .. properties.flatten_string(url) .. "\"/>"
  return res
end

function lreveal.block(content)
  return lreveal.wrap_flat_tag("p",content)
end

function lreveal.split_lr(content)
  return "<div id=\"left\">" .. content[1] .. "</div>" .. "<div id=\"right\">" .. content[2] .. "</div>"
end

function lreveal.comment(content)
  return "<!-- " .. properties.flatten_string(content) .. " -->"
end

function lreveal.raw(content)
  return properties.flatten_string(content)
end

function lreveal.tabular(tablecontent)
  res = "<table class=\"reveal\">"
  for i,row in ipairs(tablecontent) do
    res = res .. "<tr>"
    for j,cell in ipairs(row) do
      res = res .. "<td>" .. cell .. "</td>"
    end
    res = res .. "</tr>"
  end
  res = res .. "</table>"
  return res
end

return lreveal

