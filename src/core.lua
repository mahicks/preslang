-- Properties used by preslang

local core = { }
local csv_parse = require("csv_parse")

-- low-level core
core.preslang_path       = ''
core.title               = 'title'
core.bg                  = 'bgcolor'
core.bgimage             = 'bgimage'
core.bgsize              = 'bgsize'
core.style               = 'style'
core.width               = 'width'
core.height              = 'height'
core.presentation_result = "presentation"

-- keywords of the language that are implemented by backend
core.keywords = { "slides", "slide", "image", "bullet_list", "number_list",
                  "spacer", "block", "big_text", "small_text", "text" ,
                  "split_lr", "comment", "tabular", "backend" }

-- some additional methods
function core.flatten_string(content)
  local flat = ""
  if type(content) == 'table' then
    flat = ""
    -- ignore named properties by using ipairs
    for k,v in ipairs(content) do flat = flat .. v end
  else
    flat = content
  end
  return flat
end

function core.from_file(files)
  local result = ""
  for k,v in pairs(files)
  do
    local input = io.open(target,"r")
    if not (input == nil) then
      input:read(result)
    else
      io.stderr:write("Error: cannot open " .. source .. " for reading.\n")
    end
      input:close()
  end
  return result
end

function core.split_lines(text)
  local lines = {}
  for line in string.gmatch(text, "(.-)\r?\n") do
    table.insert(lines,line)
  end
  return lines
end

function core.csv_to_table(csv)
  local csvdata = core.split_lines(core.flatten_string(csv))
  local res     = { }
  for i,line in ipairs(csvdata) do
    table.insert(res,csv_parse.ParseCSVLine(line,","))
  end
  return table.unpack(res)
end

return core
