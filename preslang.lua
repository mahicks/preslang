#!/usr/bin/lua
-- Main preslang entry program
-- configuring package path
local exec_path = arg[0]:match("(.*/)")
if exec_path == nil then exec_path = "./" end
package.path = exec_path .. '/src/?.lua;' .. package.path
-- language package
local core = require "core"
core.preslang_path = exec_path
-- import backend package(s)
local reveal = require "reveal"

-- handle command line switches (and remove them)
local source  = arg[1]
local target  = arg[2]
local backend = arg[3]
for i=1,3 do -- leave only additional args
  if not (arg[1] == nil) then table.remove(arg,1) end
end

-- check for minimum input
if target == nil then
  io.stderr:write("Insufficient parameters.\n")
  io.stderr:write("Usage: ")
  io.stderr:write(arg[0] .. " <input-file> <output-file> <backend> <?pres-args>\n")
  os.exit(1)
end

-- select the backend to use
if backend == nil or _G[backend] == nil then
  print("Defaulting to reveal.js backend")
  backend = reveal -- default to reveal
else
  backend = _G[backend]
end

-- expose top-level keywords as globals
for i,keyword in pairs(core.keywords)
do
  if backend[keyword] == nil then
    print("Warning: \"" .. keyword .. "\" is missing in the selected backend")
  else
    _G[keyword] = backend[keyword]
  end
end

-- expose core as globals
for k,prop in pairs(core)
do
  if not (k == "keywords") then
    _G[k] = prop
  end
end

-- execute the final presl file and store the generated output
dofile(source) -- must define 'presentation_result' var
if not (_G[core.presentation_result] == nil) then
  output = io.open(target,"w")
  if output == nil then
    io.stderr:write("Error: cannot open " .. target .. " for writing.\n")
    os.exit(2)
  end
  output:write(_G[core.presentation_result])
  output:close()
else
  io.stderr:write("Error: file \"" .. source .. "\" does not define a presentation.\n")
  os.exit(1)
end

return 0
